use std::fmt::{Display, Formatter};

#[derive(Clone, Debug)]
pub struct TableName {
    pub name: String,
    pub schema: Option<String>,
}

impl TableName {
    pub fn new(name: &str) -> Self {
        TableName {
            name: name.to_string(),
            schema: None,
        }
    }

    pub fn with_schema(name: &str, schema: &str) -> Self {
        TableName {
            name: name.to_string(),
            schema: Some(schema.to_string()),
        }
    }
}

impl Display for TableName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self.schema {
            Some(schema) => write!(f, "{}.{}", schema, self.name),
            None => write!(f, "{}", self.name),
        }
    }
}
