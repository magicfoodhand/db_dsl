use crate::{SqlUp, TableName};

#[derive(Clone, Debug)]
pub struct Sequence {
    pub name: TableName,
    pub options: Option<SequenceOptions>,
}

impl SqlUp for Sequence {
    fn sqlite_up(&self) -> Option<String> {
        let options = self.options.clone().unwrap_or_default();
        let mut sql = format!("CREATE SEQUENCE {}", self.name);
        if let Some(start) = options.start {
            sql.push_str(&format!(" START WITH {}", start));
        }
        if let Some(increment) = options.increment {
            sql.push_str(&format!(" INCREMENT BY {}", increment));
        }
        if let Some(min) = options.min {
            sql.push_str(&format!(" MINVALUE {}", min));
        }
        if let Some(max) = options.max {
            sql.push_str(&format!(" MAXVALUE {}", max));
        }
        if let Some(cycle) = options.cycle {
            sql.push_str(&format!(" CYCLE {}", cycle));
        }
        Some(sql)
    }
}

#[derive(Clone, Debug, Default)]
pub struct SequenceOptions {
    pub start: Option<i64>,
    pub increment: Option<i64>,
    pub min: Option<i64>,
    pub max: Option<i64>,
    pub cycle: Option<bool>,
}
