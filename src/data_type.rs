use crate::{Column, SqlUp};

#[derive(Clone, Debug, Default)]
pub enum DataType {
    #[default]
    String,
    UUID,
    Text,
    Int,
    BigInt,
    Bool,
    Bits(usize),
    Float,
    Double,
    Decimal(usize, usize),
    Reference(String),
    Date,
    Datetime,
}

impl DataType {
    pub fn column(&self, name: &str) -> Column {
        Column {
            name: name.to_string(),
            data_type: Some(self.clone()),
            options: None,
        }
    }
}

impl SqlUp for DataType {
    fn sqlite_up(&self) -> Option<String> {
        let str = match self {
            DataType::UUID => "STRING".to_string(),
            DataType::String => "STRING".to_string(),
            DataType::Text => "TEXT".to_string(),
            DataType::Int => "INTEGER".to_string(),
            DataType::BigInt => "BIGINT".to_string(),
            DataType::Bool => "BOOLEAN".to_string(),
            DataType::Bits(size) => format!("BIT({})", size),
            DataType::Float => "FLOAT".to_string(),
            DataType::Double => "DOUBLE".to_string(),
            DataType::Decimal(precision, scale) => format!("DECIMAL({}, {})", precision, scale),
            DataType::Date => "DATE".to_string(),
            DataType::Datetime => "DATETIME".to_string(),
            // TODO: support all reference types
            DataType::Reference(table) => format!("BIGINT REFERENCES {}", table),
        };
        Some(str)
    }
}
