use crate::{SqlUp, TableName};
use log::warn;

#[derive(Clone, Debug, Default)]
pub struct Index {
    pub columns: Vec<String>,
    pub table_name: Option<TableName>,
    pub options: Option<IndexOptions>,
}

impl Index {
    pub fn new(columns: Vec<String>) -> Self {
        Index {
            columns,
            table_name: None,
            options: None,
        }
    }

    pub fn unique(columns: Vec<String>) -> Self {
        Index {
            columns,
            table_name: None,
            options: Some(IndexOptions {
                unique: true,
                ..Default::default()
            }),
        }
    }

    pub fn with_options(columns: Vec<String>, options: IndexOptions) -> Self {
        Index {
            columns,
            table_name: None,
            options: Some(options),
        }
    }
}

impl SqlUp for Index {
    fn sqlite_up(&self) -> Option<String> {
        let options = self.options.clone().unwrap_or_default();
        let name = if let Some(name) = &options.name {
            name.to_string()
        } else {
            self.columns.join("_")
        };
        let mut sql = format!("CREATE INDEX {}", name);
        sql.push_str(" ON ");
        if let Some(table_name) = &self.table_name {
            sql.push_str(&table_name.to_string());
        } else {
            warn!("Failed to generate index table name");
            return None;
        }
        sql.push_str(" (");
        for (i, column) in self.columns.iter().enumerate() {
            if i > 0 {
                sql.push_str(", ");
            }
            sql.push_str(column);
        }
        sql.push(')');
        Some(sql)
    }
}

#[derive(Clone, Debug, Default)]
pub struct IndexOptions {
    pub name: Option<String>,
    pub unique: bool,
}
