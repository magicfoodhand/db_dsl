use crate::{Column, ForeignKey, Index, SqlUp};

#[derive(Clone, Debug)]
pub enum TableDefinition {
    Column(Column),
    Index(Index),
    ForeignKey(ForeignKey),
}

impl TableDefinition {
    pub fn column(column: Column) -> Self {
        TableDefinition::Column(column)
    }

    pub fn index(index: Index) -> Self {
        TableDefinition::Index(index)
    }

    pub fn foreign_key(foreign_key: ForeignKey) -> Self {
        TableDefinition::ForeignKey(foreign_key)
    }
}

impl SqlUp for TableDefinition {
    fn sqlite_up(&self) -> Option<String> {
        match self {
            TableDefinition::Column(column) => column.sqlite_up(),
            TableDefinition::Index(index) => index.sqlite_up(),
            TableDefinition::ForeignKey(foreign_key) => foreign_key.sqlite_up(),
        }
    }
}
